package br.com.feirpa.sevenminutesworkout;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;

public class TelaVideos extends AppCompatActivity {

    WebView webView;
    TextView txtTitulo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_videos);

        webView = (WebView) findViewById(R.id.webView);
        txtTitulo = (TextView) findViewById(R.id.txtNomeExer);

        webView.setWebViewClient(new MyBrowser());

        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl("http://www.youtube.com/embed/" + "wRcGFgVjrag" + "?autoplay=1&vq=small");
        webView.setWebChromeClient(new WebChromeClient());

        String meuVideo = getIntent().getExtras().getString("nomeVideo");
        txtTitulo.setText(meuVideo);

    }

    private class MyBrowser extends WebViewClient {
        public boolean overrrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
}

